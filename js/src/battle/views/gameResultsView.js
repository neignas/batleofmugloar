define(
    [
    'jquery',
    'underscore',
    'backbone',
    'wNumb',
    'text!battle/templates/gameResultsTemplate.jst'
    ],
    function (
        $,
        _,
        Backbone,
        wNumb,
        GameResultsTemplate
    ) {
        "use strict";

        /**
         * @class GameResultsView
         */
        return Backbone.View.extend({

            initialize: function () {
                this.numberFormatter = wNumb({decimals: 0});
                this.victoryBattlesList = [];
                this.defeatBattlesList = [];
            },

            /**
             * @param victoryBattles
             * @returns {GameResultsView}
             */
            setVictoryBattlesList: function (victoryBattles) {
                this.victoryBattlesList = victoryBattles;
                return this;
            },

            /**
             * @param defeatBattles
             * @returns {GameResultsView}
             */
            setDefeatBattlesList: function (defeatBattles) {
                this.defeatBattlesList = defeatBattles;
                return this;
            },

            /**
             * @private
             * @param victoryCount
             * @param defeatCount
             * @returns {int}
             */
            getWarSuccessRating: function (victoryCount, defeatCount) {
                var battlesCount = victoryCount + defeatCount;
                return this.numberFormatter.to((victoryCount / battlesCount) * 100);
            },

            /**
             * @param gameSuccessRatio
             * @private
             * @returns {boolean}
             */
            isGameVictoryReached: function (gameSuccessRatio) {
                return gameSuccessRatio >= this.constructor.ACCEPTED_SUCCESS_RATIO;
            },

            /**
             * Renders war (many battles) results
             */
            render: function () {
                var warSuccessRating = this.getWarSuccessRating(
                    this.victoryBattlesList.length,
                    this.defeatBattlesList.length
                );

                this.$el.html(
                    _.template(
                        GameResultsTemplate,
                        {variable: 'data'}
                    )({
                        victoryBattles: this.victoryBattlesList,
                        defeatBattles: this.defeatBattlesList,
                        successRatio: warSuccessRating,
                        gameVictoryReached: this.isGameVictoryReached(warSuccessRating)
                    })
                );
            }
        }, {
            ACCEPTED_SUCCESS_RATIO: 60
        });
    }
);
