define(
    [
    'underscore',
    'backbone',
    'wNumb',
    'battle/models/dragonModel',
    'text!battle/templates/dragonsChooserTemplate.jst'
    ],
    function (
        _,
        Backbone,
        wNumb,
        DragonModel,
        DragonsChooserTemplate
    ) {
        "use strict";

        /**
         * @class DragonsChooserView
         */
        return Backbone.View.extend({

            events: {
                "click .select-dragon": "onDragonSelected"
            },

            initialize: function () {
                this.selectedDragon = DragonModel.KIND_STRONG; // select by default strong dragon
            },

            /**
             * @private
             */
            onDragonSelected: function (clickEvent) {
                clickEvent.preventDefault();

                var selectedDragon = this.$(clickEvent.target)
                    .closest('.card-block')
                    .find('.selected-dragon')
                    .val();

                switch (selectedDragon) {
                    case DragonModel.KIND_STRONG:
                        this.selectedDragon = DragonModel.KIND_STRONG;
                        break;
                    case DragonModel.KIND_MEDIUM:
                        this.selectedDragon = DragonModel.KIND_MEDIUM;
                        break;
                    default:
                        throw new Error('Unknown dragon kind: ' + selectedDragon);
                        break;
                }

                this.render();
            },

            disable: function () {
                this.$('.select-dragon').prop('disabled', true);
            },

            enable: function () {
                this.$('.select-dragon').prop('disabled', false);
            },

            /**
             * Renders dragons chooser view
             */
            render: function () {
                this.$el.html(
                    _.template(
                        DragonsChooserTemplate,
                        {variable: 'data'}
                    )({
                        selectedDragon: this.selectedDragon,
                        mediumDragonKind: DragonModel.KIND_MEDIUM,
                        strongDragonKind: DragonModel.KIND_STRONG
                    })
                );

                this.trigger('dragon:change', this.selectedDragon);
            }
        });
    }
);
