define(
    [
    'underscore',
    'backbone',
    'wNumb',
    'text!battle/templates/gameProgressTemplate.jst'
    ],
    function (
        _,
        Backbone,
        wNumb,
        GameProgressTemplate
    ) {
        "use strict";

        /**
         * @class GameProgressView
         */
        return Backbone.View.extend({

            initialize: function () {
                this.numberFormatter = wNumb({decimals: 0});
                this.victoryBattlesCount = 0;
                this.defeatBattlesCount = 0;
                this.totalBattlesCount = 0;
            },

            /**
             * @param victoryBattlesCount
             * @returns {GameProgressView}
             */
            setVictoryBattlesCount: function (victoryBattlesCount) {
                this.victoryBattlesCount = victoryBattlesCount;
                return this;
            },

            /**
             * @param defeatBattlesCount
             * @returns {GameProgressView}
             */
            setDefeatBattlesCount: function (defeatBattlesCount) {
                this.defeatBattlesCount = defeatBattlesCount;
                return this;
            },

            /**
             * @param totalBattlesCount
             * @returns {GameProgressView}
             */
            setTotalBattlesCount: function (totalBattlesCount) {
                this.totalBattlesCount = totalBattlesCount;
                return this;
            },

            /**
             * @private
             */
            calculateProgress: function () {
                var battlesProcessed = this.defeatBattlesCount + this.victoryBattlesCount;

                // avoid cast to integer, just simply check
                if (this.totalBattlesCount < 1) {
                    return 0;
                }

                return this.numberFormatter.to((battlesProcessed / this.totalBattlesCount) * 100);
            },

            /**
             * Renders war (many battles) results
             */
            render: function () {
                this.$el.html(
                    _.template(
                        GameProgressTemplate,
                        {variable: 'data'}
                    )({
                        progressInPercents: this.calculateProgress()
                    })
                );
            }
        });
    }
);
