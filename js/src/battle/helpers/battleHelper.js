define(
    [
        'jquery',
        'underscore',
        'backbone',
        'routes',
        'battle/models/knightModel',
        'battle/models/dragonModel',
        'battle/models/strongDragonModel',
        'battle/models/battleResolverModel',
        'battle/models/battleModel',
        'battle/models/weatherModel'
    ],
    function (
        $,
        _,
        Backbone,
        routes,
        KnightModel,
        DragonModel,
        StrongDragonModel,
        BattleResolverModel,
        BattleModel,
        WeatherModel
    ) {
        'use strict';

        /**
         * @constructor
         */
        var BattleHelper = function () {
            this.selectedDragonKind = null;
            return this;
        };

        /**
         * Configures dragons to make them able to fight with knights by knights features
         * Splits games to chunks
         *
         * @class BattleHelper
         */
        _.extend(BattleHelper.prototype, Backbone.Events, {

            /**
             * @param {int} gameId
             * @param {KnightModel} knightModel
             * @param {WeatherModel} weatherForBattle
             * @returns {DragonModel}
             */
            getDragonForBattle: function (gameId, knightModel, weatherForBattle) {
                var dragon;

                if (this.selectedDragonKind == DragonModel.KIND_STRONG) {
                    dragon = new StrongDragonModel();
                }
                if (this.selectedDragonKind == DragonModel.KIND_MEDIUM) {
                    dragon = new DragonModel();
                }

                if (_.isEmpty(dragon)) {
                    throw new Error('DragonModel missed in getDragonForBattle.');
                }

                dragon.setWingStrength(knightModel.getAgility());
                dragon.setClawSharpness(knightModel.getArmor());
                dragon.setScaleThickness(knightModel.getAttack());
                dragon.setFireBreath(knightModel.getEndurance());

                dragon.setSelectedKind(this.selectedDragonKind);

                dragon.setDailyFeatures(); // normal weathers

                if (dragon.isMedium()) {
                    return dragon;
                }

                // other features available for strong dragons
                switch (weatherForBattle.getCode()) {
                    case WeatherModel.WEATHER_CODE_HEAVY_RAINS:
                        dragon.setHeavyRainFeatures();
                        break;
                    case WeatherModel.WEATHER_CODE_LONG_DRY:
                        dragon.setLongDryFeatures();
                        break;
                    case WeatherModel.WEATHER_CODE_STORM:
                        dragon.disableDragon();
                        break;
                }

                return dragon;
            },

            /**
             * @param battlesToFetch
             * @returns {Array}
             */
            getWeatherModelList: function (battlesToFetch) {
                var weatherResolverModel,
                    battlesWeatherList = [];

                _.each(battlesToFetch, function (battle) {
                    weatherResolverModel = new WeatherModel({}, {urlTemplate: routes.weatherUrl});
                    weatherResolverModel
                        .setGameId(battle.getGameId());

                    battlesWeatherList.push(weatherResolverModel);
                }.bind(this));

                return battlesWeatherList;
            },

            /**
             * @private
             * @param gameId
             * @param battlesWeatherList
             */
            getWeatherFroBattle: function (gameId, battlesWeatherList) {
                return _.find(battlesWeatherList, function (battleWeather) {
                    return battleWeather.getGameId() == gameId;
                });
            },

            /**
             * @private
             * @param battlesToFetch all battles to be fetched
             * @param battlesWeatherList weather for battles to be fetched
             */
            getBattleResolverModelList: function (battlesToFetch, battlesWeatherList) {
                var dragonModel,
                    battleResolverModel,
                    battlesToSolve = [],
                    weatherForBattle;

                _.each(battlesToFetch, function (battle) {
                    weatherForBattle = this.getWeatherFroBattle(battle.getGameId(), battlesWeatherList);

                    dragonModel = this.getDragonForBattle(
                        battle.getGameId(),
                        battle.getKnight(),
                        weatherForBattle
                    );

                    battleResolverModel = new BattleResolverModel({}, {urlTemplate: routes.solveBattleUrl});
                    battleResolverModel.setGameId(battle.getGameId());

                    if (!dragonModel.isDisabled()) {
                        battleResolverModel.setDragon(dragonModel);
                    }

                    battlesToSolve.push(battleResolverModel);
                }.bind(this));

                return battlesToSolve;
            },

            /**
             * @param battlesCount
             * @returns {$.Deferred}
             */
            resolveBattles: function (battlesCount) {
                var battleModel,
                    battlesToFetch = [],
                    battlesCompleted;

                _(battlesCount).times(function () {
                    battleModel = new BattleModel({}, {urlRoot: routes.battleUrl});
                    battlesToFetch.push(battleModel);
                });

                battlesCompleted = _.invoke(battlesToFetch, 'fetch');

                //when all of them are completed...
                return $.when.apply($, battlesCompleted).then(function () {
                    var battlesToSolve = [],
                        battlesWeatherList,
                        battlesSolved,
                        weatherSolved;

                    battlesWeatherList = this.getWeatherModelList(battlesToFetch);

                    weatherSolved = _.invoke(battlesWeatherList, 'fetch');

                    // resolve weather for each battle
                    return $.when.apply($, weatherSolved).then(function () {

                        battlesToSolve = this.getBattleResolverModelList(
                            battlesToFetch,
                            battlesWeatherList
                        );

                        battlesSolved = _.invoke(battlesToSolve, 'save');

                        return $.when.apply($, battlesSolved).then(function () {
                            _.each(battlesToSolve, function(resolvedBattleModel) {

                                if (!resolvedBattleModel.isVictory() && !resolvedBattleModel.isDefeat()) {
                                    throw new Error('Unknown battle resolve status: ' + resolvedBattleModel.getStatus());
                                }

                                if (resolvedBattleModel.isVictory()) {
                                    this.trigger('resolveBattles:victory', resolvedBattleModel.getGameId());
                                }

                                if (resolvedBattleModel.isDefeat()) {
                                    this.trigger(
                                        'resolveBattles:defeat',
                                        resolvedBattleModel.getGameId(),
                                        resolvedBattleModel.getMessage()
                                    );
                                }

                                this.trigger('resolveBattles:battleResolved');
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            },

            /**
             * Splits total battles count into chunks to easily handle asynchronous ajax requests
             *
             * @param gamesCount
             * @param chunkSize
             * @returns {Array}
             */
            getGameChunkList: function (gamesCount, chunkSize) {
                var gameChunkList = [],
                    i = chunkSize;

                while (i < gamesCount + chunkSize) {
                    if (i >  gamesCount) {
                        gameChunkList.push(gamesCount - (i - chunkSize));
                        break;
                    }

                    gameChunkList.push(chunkSize);
                    i += chunkSize;
                }

                return gameChunkList;
            },

            /**
             * sets selected dragon kind
             * @param dragonKind
             */
            setSelectedDragonKind: function (dragonKind) {
                this.selectedDragonKind = dragonKind;
            }
        });

        return BattleHelper;
    }
);
