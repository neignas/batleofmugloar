define(
    [
        'backbone'
    ],
    function (
        Backbone
    ) {
        'use strict';

        /**
         * @class KnightModel
         */
        return Backbone.Model.extend({
            defaults: {
                name: null,
                attack: null,
                armor: null,
                agility: null,
                endurance: null
            },

            getName: function () {
                return this.get('name');
            },

            getAttack: function () {
                return this.get('attack');
            },

            getArmor: function () {
                return this.get('armor');
            },

            getAgility: function () {
                return this.get('agility');
            },

            getEndurance: function () {
                return this.get('endurance');
            }
        });
    }
);
