define(
    [
        'backbone'
    ],
    function (
        Backbone
    ) {
        'use strict';

        /**
         * @class BattleResolverModel
         */
        return Backbone.Model.extend({
            idAttribute: 'gameId',

            defaults: {
                gameId: null,
                dragon: null
            },

            initialize: function (attributes, options) {
                this.urlRoot = null;
                this.urlTemplate = null;

                if (options && options.urlTemplate) {
                    this.urlTemplate = options.urlTemplate || null;
                }
            },

            /**
             * @param gameId
             * @returns {BattleResolverModel}
             */
            setGameId: function (gameId) {
                this.set('gameId', gameId);
                return this;
            },

            /**
             * @returns {int}
             */
            getGameId: function () {
                return this.get('gameId');
            },

            /**
             * @param {DragonModel} dragonModel
             * @returns {BattleResolverModel}
             */
            setDragon: function (dragonModel) {
                if (!(dragonModel instanceof Backbone.Model)) {
                    throw new Error("BattleResolverModel needs dragonModel in setDragon function.");
                }

                this.set('dragon', dragonModel);
                return this;
            },

            /**
             * Returns resolved game status Defeat/Victory
             * @returns {string}
             */
            getStatus: function () {
                return this.get('status');
            },

            /**
             * Returns resolved game message on defeat
             * @private
             * @returns {string}
             */
            getMessage: function () {
                return this.get('message');
            },

            /**
             * @returns {boolean}
             */
            isDefeat: function () {
                return this.getStatus() === this.constructor.GAME_STATUS_DEFEAT;
            },

            /**
             * @returns {boolean}
             */
            isVictory: function () {
                return this.getStatus() === this.constructor.GAME_STATUS_VICTORY;
            },

            url: function () {
                if (null === this.urlTemplate) {
                    throw new Error('BattleResolverModel url template is mandatory.');
                }

                if (this.get('gameId') < 1) {
                    throw new Error('BattleResolverModel game id is mandatory.');
                }

                return this.urlTemplate.replace('__game_id__', this.get('gameId'));
            }
        }, {
            GAME_STATUS_DEFEAT: 'Defeat',
            GAME_STATUS_VICTORY: 'Victory'
        });
    }
);
