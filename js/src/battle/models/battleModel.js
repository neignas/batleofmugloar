define(
    [
        'backbone',
        'battle/models/knightModel'
    ],
    function (
        Backbone,
        KnightModel
    ) {
        'use strict';

        /**
         * @class BattleModel
         */
        return Backbone.Model.extend({

            defaults: {
                gameId: null
            },

            initialize: function (attributes, options) {
                this.urlRoot = null;

                if (options.urlRoot) {
                    this.urlRoot = options.urlRoot;
                }
            },

            /**
             * @param response
             * @returns {Object} json
             */
            parse: function(response){
                response.knight = new KnightModel(response.knight, {parse: true});
                return response;
            },

            /**
             * @returns {int}
             */
            getGameId: function () {
                return this.get('gameId');
            },

            /**
             * @returns {KnightModel}
             */
            getKnight: function () {
                return this.get('knight');
            },

            /**
             * @returns {string}
             */
            url: function () {
                if (null === this.urlRoot) {
                    console.error('BattleModel "urlRoot" is not defined');
                }

                return this.urlRoot;
            }
        });
    }
);
