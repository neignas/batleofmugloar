define(
    [
        'underscore',
        'backbone'
    ],
    function (
        _,
        Backbone
    ) {
        'use strict';

        /**
         * @class DragonModel
         */
        return Backbone.Model.extend({

            defaults: {
                clawSharpness: null,
                fireBreath: null,
                scaleThickness: null,
                wingStrength: null
            },

            initialize: function (model, options) {
                this.changedAttributes = [];
                this.selectedKind = null;
                this.disabled = false;
            },

            /**
             * sets Dragon features to fight in normal weather
             */
            setDailyFeatures: function () {
                this.increaseBiggestFeature(2);
                this.decreaseSmallestFeatures();
            },

            /**
             * Disables dragon to not send him to battle
             */
            disableDragon: function () {
                this.disabled = true;
            },

            /**
             * Checks if send dragon to battle
             * @returns {boolean}
             */
            isDisabled: function () {
                return this.disabled;
            },

            /**
             * Sets features to fight in heavy rains
             */
            setHeavyRainFeatures: function () {
                throw new Error("'setHeavyRainFeatures' is not configured for this dragon type");
            },

            setSelectedKind: function (selectedKind) {
                this.selectedKind = selectedKind;
            },

            /**
             * Sets features to fight in long dry weather
             */
            setLongDryFeatures: function () {
                throw new Error("'setLongDryFeatures' is not configured for this dragon type");
            },

            /**
             * As knights maximum skill point is 8, we simply do not check for MAXIMUM_SKILL_VALUE
             * by adding +2 to highest knight skill
             *
             * @private
             * @param increaseCount
             */
            increaseBiggestFeature: function (increaseCount) {
                var biggestFeatureValue = 0,
                    biggestFeatureName = '';

                _.each(this.attributes, function (attributeValue, attributeName) {
                    if (attributeValue > biggestFeatureValue) {
                        biggestFeatureValue = attributeValue;
                        biggestFeatureName = attributeName;
                    }
                });

                this.changedAttributes.push(biggestFeatureName);

                this.set(biggestFeatureName, biggestFeatureValue + increaseCount);
            },

            /**
             * @private
             */
            decreaseSmallestFeatures: function () {
                while (this.sumFeatures() > this.constructor.MAXIMUM_SKILLS_COUNT) {
                    this.decreaseSmallestFeature();
                }
            },

            /**
             * @private
             */
            decreaseSmallestFeature: function () {
                var smallestFeatureValue = this.constructor.MAXIMUM_SKILL_VALUE,
                    smallestFeatureName = '',
                    decreaseValue = 1;

                _.each(this.attributes, function (attributeValue, attributeName) {
                    if (attributeValue < smallestFeatureValue
                        && attributeValue > 0
                        && this.changedAttributes.indexOf(attributeName) === -1
                    ) {
                        smallestFeatureValue = attributeValue;
                        smallestFeatureName = attributeName;
                    }
                }.bind(this));

                this.changedAttributes.push(smallestFeatureName);

                this.set(smallestFeatureName, smallestFeatureValue - decreaseValue);
            },

            /**
             * @private
             * @returns {int}
             */
            sumFeatures: function () {
                return this.getClawSharpness()
                    + this.getFireBreath()
                    + this.getScaleThickness()
                    + this.getWingStrength();
            },

            getClawSharpness: function () {
                return this.get('clawSharpness');
            },

            getFireBreath: function () {
                return this.get('fireBreath');
            },

            getScaleThickness: function () {
                return this.get('scaleThickness');
            },

            getWingStrength: function () {
                return this.get('wingStrength');
            },

            setClawSharpness: function (clawSharpness) {
                this.set('clawSharpness', clawSharpness);
                return this;
            },

            setFireBreath: function (fireBreath) {
                this.set('fireBreath', fireBreath);
                return this;
            },

            setScaleThickness: function (scaleThickness) {
                this.set('scaleThickness', scaleThickness);
                return this;
            },

            setWingStrength: function (wingStrength) {
                this.set('wingStrength', wingStrength);
                return this;
            },

            isStrong: function () {
                return this.selectedKind === this.constructor.KIND_STRONG;
            },

            isMedium: function () {
                return this.selectedKind === this.constructor.KIND_MEDIUM;
            }
        }, {
            MAXIMUM_SKILLS_COUNT: 20,
            MAXIMUM_SKILL_VALUE: 10,
            KIND_STRONG: 'strong',
            KIND_MEDIUM: 'medium'
        });
    }
);
