define(
    [
        'underscore',
        'battle/models/dragonModel'
    ],
    function (
        _,
        DragonModel
    ) {
        'use strict';

        /**
         * @class StrongDragonModel
         * @extends {Backbone.Model}
         */
        return DragonModel.extend({

            /**
             * Sets features to fight in heavy rains
             */
            setHeavyRainFeatures: function () {
                this.setWingStrength(this.constructor.MAXIMUM_SKILL_VALUE);
                this.setClawSharpness(this.constructor.MAXIMUM_SKILL_VALUE);
                this.setScaleThickness(0);
                this.setFireBreath(0);
            },

            /**
             * Sets features to fight in long dry weather
             */
            setLongDryFeatures: function () {
                var wellBalancedValue = this.constructor.MAXIMUM_SKILL_VALUE / 2;

                this.setWingStrength(wellBalancedValue);
                this.setClawSharpness(wellBalancedValue);
                this.setScaleThickness(wellBalancedValue);
                this.setFireBreath(wellBalancedValue);
            }
        });
    }
);
