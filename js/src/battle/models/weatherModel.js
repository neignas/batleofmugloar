define(
    [
        'backbone'
    ],
    function (
        Backbone
    ) {
        'use strict';

        /**
         * @class WeatherModel
         */
        return Backbone.Model.extend({
            idAttribute: 'gameId',

            defaults: {
                gameId: null
            },

            initialize: function (attributes, options) {
                this.urlRoot = null;
                this.urlTemplate = null;

                if (options && options.urlTemplate) {
                    this.urlTemplate = options.urlTemplate || null;
                }
            },

            /**
             * Gets code retrieved from royal weather service
             * @returns {string}
             */
            getCode: function () {
                return this.get('code');
            },

            /**
             * @param gameId
             * @returns {WeatherModel}
             */
            setGameId: function (gameId) {
                this.set('gameId', gameId);
                return this;
            },

            fetch: function(options) {
                options || (options = {});
                options.dataType = 'xml'; // weather server should return xml response
                return Backbone.Model.prototype.fetch.call(this, options);
            },

            parse: function (data) {
                var xml = $(data);
                return {code: xml.find('report').find('code').text()};
            },

            /**
             * @returns {int}
             */
            getGameId: function () {
                return this.get('gameId');
            },

            url: function () {
                if (null === this.urlTemplate) {
                    throw new Error('WeatherModel url template is mandatory.');
                }

                if (this.get('gameId') < 1) {
                    throw new Error('WeatherModel game id is mandatory.');
                }

                return this.urlTemplate.replace('__game_id__', this.get('gameId'));
            }
        }, {
            WEATHER_CODE_NORMAL: 'NMR', // normal weather for normal fights
            WEATHER_CODE_HEAVY_RAINS: 'HVA', // heavy rains, knights use umbrella boats
            WEATHER_CODE_LONG_DRY: 'T E', // long dry
            WEATHER_CODE_STORM: 'SRO' // storm
        });
    }
);
