define(
    [
        'jquery',
        'app/views/gameInitView'
    ],

    /**
     * @param $
     * @param GameInitView
     * @returns {{initialize: Function}}
     */
    function($, GameInitView) {
        "use strict";

        var initialize = function(args) {
            if (!args.hasOwnProperty('container')) {
                throw new Error("GameInit should have 'container' attribute defined");
            }

            var gameInitView = new GameInitView({
                el: args.container
            });

            gameInitView.render();
        };

        return {
            initialize: initialize
        };
    }
);
