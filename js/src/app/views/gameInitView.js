define(
    [
    'jquery',
    'underscore',
    'backbone',
    'routes',
    'nouislider',
    'wNumb',
    'battle/views/gameResultsView',
    'battle/views/gameProgressView',
    'battle/views/dragonsChooserView',
    'battle/helpers/battleHelper',
    'text!app/templates/gameInitTemplate.jst'
    ],
    function (
        $,
        _,
        Backbone,
        routes,
        NoUiSlider,
        wNumb,
        GameResultsView,
        GameProgressView,
        DragonsChooserView,
        BattleHelper,
        GameInitTemplate
    ) {
        "use strict";

        /**
         * @class GameInitView
         */
        return Backbone.View.extend({

            events: {
                "click .go-to-war": "onGoToGameClick"
            },

            initialize: function () {
                this.dragonsCountElement = null;
                this.battleChunkSizeElement = null;
                this.defeatList = [];
                this.victoryList = [];

                this.battleHelper = new BattleHelper({
                    solveBattleUrl: routes.solveBattleUrl
                });

                this.gameResultsView = new GameResultsView();
                this.gameProgressView = new GameProgressView();
                this.dragonsChooserView = new DragonsChooserView();

                this.battleHelper.on('resolveBattles:victory', function (gameId) {
                    this.victoryList.push(gameId);
                }.bind(this));

                this.battleHelper.on('resolveBattles:defeat', function (gameId, message) {
                    this.defeatList.push({
                        gameId: gameId,
                        message: message
                    });
                }.bind(this));

                this.battleHelper.on('resolveBattles:battleResolved', function () {
                    this.updateProgressBar();
                }.bind(this));

                this.dragonsChooserView.on('dragon:change', function (dragonKind) {
                    this.battleHelper.setSelectedDragonKind(dragonKind);
                }.bind(this));
            },

            /**
             * @private
             * @param clickEvent
             */
            onGoToGameClick: function (clickEvent) {
                clickEvent.preventDefault();

                var battlesCount,
                    gameChunks,
                    chunkPromise = $.Deferred(),
                    gameChunksCount;

                this.disableGameOptions();
                this.resetGameResults();

                battlesCount = this.getDragonsCount();

                this.gameProgressView.setTotalBattlesCount(battlesCount);
                this.updateProgressBar();

                // split game count into chunks, to not fire all games at once asynchronously but
                // fire some games (number is set in UI) at once to avoid browser crashes
                gameChunks = this.battleHelper.getGameChunkList(battlesCount, this.getBattlesChunkSize());
                gameChunksCount = gameChunks.length;

                chunkPromise.resolve();

                // resolve game chunks synchronously
                _.each(gameChunks, function (gamesInChunk, index) {
                    chunkPromise = chunkPromise.then(function() {
                        var resolveBattlesPromise = this.battleHelper.resolveBattles(gamesInChunk);

                        // if last iteration in game chunks, show results
                        if (index + 1 == gameChunksCount) {
                            resolveBattlesPromise.then(function () {
                                this.showGameResults();
                            }.bind(this));
                        }

                        return resolveBattlesPromise;
                    }.bind(this));
                }.bind(this));
            },

            /**
             * @private
             */
            getDragonsCount: function () {
                return this.dragonsCountElement.noUiSlider.get().replace(' ', '');
            },

            /**
             * @private
             */
            getBattlesChunkSize: function () {
                return +this.battleChunkSizeElement.noUiSlider.get(); // cast value to int
            },

            /**
             * @private
             * @returns {GameInitView}
             */
            showGameResults: function () {
                this.gameResultsView
                    .setDefeatBattlesList(this.defeatList)
                    .setVictoryBattlesList(this.victoryList)
                    .render();

                this.enableGameOptions();

                return this;
            },

            /**
             * @private
             * @returns {GameInitView}
             */
            updateProgressBar: function () {
                this.gameProgressView
                    .setDefeatBattlesCount(this.defeatList.length)
                    .setVictoryBattlesCount(this.victoryList.length)
                    .render();

                return this;
            },

            /**
             * Renders main game view
             */
            render: function () {
                this.$el.html(
                    _.template(
                        GameInitTemplate,
                        {
                        },
                        {variable: 'data'}
                    )
                );

                this.gameResultsView
                    .setElement(this.$('.game-results'));

                this.gameProgressView
                    .setElement(this.$('.game-progress'));

                this.dragonsChooserView
                    .setElement(this.$('.dragons-chooser'))
                    .render();

                this.showBattlesCountSlider();
                this.showBattleChunkSizeSlider();
            },

            /**
             * @private
             */
            showBattlesCountSlider: function () {
                // get native DOM element from jQuery selector
                this.dragonsCountElement = this.$('#dragonsCount')[0];

                NoUiSlider.create(this.dragonsCountElement, {
                    start: [11],
                    connect: 'lower',
                    step: 1,
                    range: {
                        'min': [1],
                        'max': [10000]
                    },
                    format: wNumb({
                        decimals: 0,
                        thousand: ' '
                    })
                });

                this.dragonsCountElement.noUiSlider.on('update', function( values, handle ) {
                    this.$('#dragonsEntered').html(values[handle]);
                }.bind(this));
            },

            /**
             * @private
             */
            showBattleChunkSizeSlider: function () {
                // get native DOM element from jQuery selector
                this.battleChunkSizeElement = this.$('#battlesChunks')[0];

                NoUiSlider.create(this.battleChunkSizeElement, {
                    start: [10],
                    connect: 'lower',
                    step: 10,
                    range: {
                        'min': [10],
                        'max': [400]
                    },
                    format: wNumb({
                        decimals: 0
                    })
                });

                this.battleChunkSizeElement.noUiSlider.on('update', function( values, handle ) {
                    this.$('#battlesChunksSelected').html(values[handle]);
                }.bind(this));
            },

            /**
             * @private
             */
            disableGameOptions: function () {
                this.$('.go-to-war').prop('disabled', true);
                this.$('.go-to-war .fa').removeClass('fa-flash').addClass('fa-cog fa-spin');
                this.dragonsCountElement.setAttribute('disabled', 'true');
                this.battleChunkSizeElement.setAttribute('disabled', 'true');
                this.dragonsChooserView.disable();
            },

            /**
             * @private
             */
            enableGameOptions: function () {
                this.$('.go-to-war').prop('disabled', false);
                this.$('.go-to-war .fa').removeClass('fa-cog fa-spin').addClass('fa-flash');
                this.dragonsCountElement.removeAttribute('disabled');
                this.battleChunkSizeElement.removeAttribute('disabled');
                this.dragonsChooserView.enable();
            },

            /**
             * @private
             * @returns {GameInitView}
             */
            resetGameResults: function () {
                this.defeatList = [];
                this.victoryList = [];

                this.gameProgressView.$el.empty();
                this.gameResultsView.$el.empty();

                return this;
            }
        });
    }
);
