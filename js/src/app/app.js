/*global $, define, require, JS_VERSION*/
require.config({
    baseUrl: 'js/src',
    urlArgs: "v=" +  JS_VERSION,
    paths: {
        jquery: '../vendor/jquery/jquery.min',
        bootstrap: '../vendor/bootstrap/bootstrap.min',
        underscore: '../vendor/underscore/underscore-min',
        backbone: '../vendor/backbone/backbone-min',
        text: "../vendor/text/text",
        nouislider: "../vendor/nouislider/nouislider.min",
        wNumb: "../vendor/wnumb/wNumb"
    },
    shim: {
        jquery: {
            exports: '$'
        },
        bootstrap: {
            deps: ['jquery']
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore']
        },
        fitText: {
            deps: ['jquery']
        }
    }
});

define('routes', {
    battleUrl: 'http://www.dragonsofmugloar.com/api/game',
    solveBattleUrl: 'http://www.dragonsofmugloar.com/api/game/__game_id__/solution',
    weatherUrl: 'http://www.dragonsofmugloar.com/weather/api/report/__game_id__'
});

define(
    [
        'app/gameInit'
    ],
    function (
        GameInit
    ) {
        'use strict';

        GameInit.initialize({
            container: $('.game-container')
        });
    }
);
