# Intro #

Simple Battle of Mugloar application implemented to solve [www.dragonsofmugloar.com](http://www.dragonsofmugloar.com) restful api game.

It's implemented with JavaScript as single page app.

Used components:

* Backbone
* Underscore
* Requirejs
* Boostrap
* Text (for underscore templates)
* Nouislider (for value slider/scrooler)
* Wnumb (as number formatter)
* Font-Awesome (for awesome icons)
* Node and packages(for javascript testing and dependencies management):
    * chai (for assertion)
    * mocha
    * phantomjs (to test UI)
    * mocha-phantomjs
    * sinon (to mock UI requests)
* BASH simple script to run javascript tests

### Where to find? ###

* Git repository available here [https://bitbucket.org/neignas/batleofmugloar/](https://bitbucket.org/neignas/batleofmugloar/)
* Public demo available on [http://battleofmugloar.nedzinskas.com](http://battleofmugloar.nedzinskas.com)

### How do I get set up? ###

This game is based on single page app, Your server should handle just .html files.

* Copy source files. How to clone git repository [https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) 
* Install node [https://docs.npmjs.com/getting-started/installing-node](https://docs.npmjs.com/getting-started/installing-node)
* Install bower [https://bower.io/#install-bower](https://bower.io/#install-bower)
* Install bower-installer [https://www.npmjs.com/package/bower-installer](https://www.npmjs.com/package/bower-installer)
* Go to project root (where index.html is located)
* Run ```bower-installer```
* Configure Your hosts, in Your hosts file add: ```127.0.0.1 battle.localhost```
* After that add Your new host ```battle.localhost``` to web server virtual hosts
* nginx to serve .html files:
```
server { 
     listen 80; 
     server_name battle.localhost; 
     root www_path_to_project_files; 
     location / {
       try_files $uri $uri/index.html;
     }
 }
```
* Enjoy ```battle.localhost``` :)

### Running tests ###

* ```cd test``` (from project root)
* ```node install``` will install node packages for javascript testing
* ```cd ../``` return to project root
* ```bin/jstests``` runs javascript tests

### Further thoughts ###

* Add error handling on server requests failures