define(
    [
    'underscore',
    'chai',
    'battle/helpers/battleHelper',
    'battle/models/knightModel',
    'battle/models/weatherModel',
    'battle/models/dragonModel'
    ],
    function (
        _,
        chai,
        BattleHelper,
        KnightModel,
        WeatherModel,
        DragonModel
    ) {
        "use strict";

        describe("Battle helper test", function () {

            var assert = chai.assert,
                helper;

            function getKnight() {
                return new KnightModel({
                    "name": "Sir. Russell Jones of Alberta",
                    "attack": 2,
                    "armor": 7,
                    "agility": 3,
                    "endurance": 8
                });
            }

            beforeEach(function () {
                helper = new BattleHelper();
            });

            it("should get dragon for normal battle", function () {
                var weatherModel = new WeatherModel({
                        gameId: 1,
                        code: 'NMR'
                    }),
                    dragon;

                helper.setSelectedDragonKind(DragonModel.KIND_STRONG);
                dragon = helper.getDragonForBattle(1, getKnight(), weatherModel);

                assert.equal(dragon.getClawSharpness(), 7);
                assert.equal(dragon.getFireBreath(), 10);
                assert.equal(dragon.getScaleThickness(), 1);
                assert.equal(dragon.getWingStrength(), 2);
            });

            it("should get dragon for heavy rain battle", function () {
                var weatherModel = new WeatherModel({
                        gameId: 1,
                        code: WeatherModel.WEATHER_CODE_HEAVY_RAINS
                    }),
                    dragon;

                helper.setSelectedDragonKind(DragonModel.KIND_MEDIUM);
                dragon = helper.getDragonForBattle(1, getKnight(), weatherModel);

                // easy dragon should be set
                assert.equal(dragon.getClawSharpness(), 7);
                assert.equal(dragon.getFireBreath(), 10);
                assert.equal(dragon.getScaleThickness(), 1);
                assert.equal(dragon.getWingStrength(), 2);

                // only strong dragons should know the weather
                helper.setSelectedDragonKind(DragonModel.KIND_STRONG);
                dragon = helper.getDragonForBattle(1, getKnight(), weatherModel);

                assert.equal(dragon.getClawSharpness(), 10);
                assert.equal(dragon.getWingStrength(), 10);
                assert.equal(dragon.getFireBreath(), 0);
                assert.equal(dragon.getScaleThickness(), 0);
            });

            it("should get dragon for long dry battle", function () {
                var weatherModel = new WeatherModel({
                        gameId: 1,
                        code: WeatherModel.WEATHER_CODE_LONG_DRY
                    }),
                    dragon;

                // only strong dragons should know the weather
                helper.setSelectedDragonKind(DragonModel.KIND_STRONG);
                dragon = helper.getDragonForBattle(1, getKnight(), weatherModel);

                assert.equal(dragon.getClawSharpness(), 5);
                assert.equal(dragon.getWingStrength(), 5);
                assert.equal(dragon.getFireBreath(), 5);
                assert.equal(dragon.getScaleThickness(), 5);
            });

            it("should split battles in chunks", function () {
                var chunkList;

                chunkList = helper.getGameChunkList(5, 1);
                assert.equal(chunkList.length, 5);

                chunkList = helper.getGameChunkList(15, 3);
                assert.equal(chunkList.length, 5);

                chunkList = helper.getGameChunkList(101, 10);
                assert.equal(chunkList.length, 11);
                // last chunk should have one element
                assert.equal(chunkList[chunkList.length - 1], 1);
            });
        });
    }
);
