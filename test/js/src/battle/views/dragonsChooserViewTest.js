define(
    [
    'underscore',
    'chai',
    'battle/views/dragonsChooserView',
    'battle/models/dragonModel'
    ],
    function (
        _,
        chai,
        DragonsChooserView,
        DragonModel
    ) {
        "use strict";

        describe("Dragons chooser view", function () {

            var assert = chai.assert,
                view;

            beforeEach(function () {
                view = new DragonsChooserView();
            });

            afterEach(function () {
                view.remove();
            });

            it("should render it self", function () {
                view.render();
            });

            it("should disable elements on disable", function () {
                view.render();
                view.disable();
                assert.ok(view.$('.select-dragon').is(':disabled'), 'Dragons selector should be disabled');
            });

            it("should select dragon", function () {
                view.render();

                view.$('.select-dragon:eq(1)').click();
                assert.equal(view.selectedDragon, DragonModel.KIND_MEDIUM);
            });
        });
    }
);
