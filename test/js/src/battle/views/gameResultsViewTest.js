define(
    [
    'underscore',
    'chai',
    'battle/views/gameResultsView'
    ],
    function (
        _,
        chai,
        GameResultsView
    ) {
        "use strict";

        describe("Game results view", function () {

            var assert = chai.assert,
                view;

            beforeEach(function () {
                view = new GameResultsView();
            });

            afterEach(function () {
                view.remove();
            });

            it("should render it self", function () {
                view.render();
            });

            it("should render results", function () {
                view.setVictoryBattlesList([1, 2, 3, 4, 5, 8, 10]);
                view.setDefeatBattlesList([
                    {gameId: 6, message: 'Knight was stronger'},
                    {gameId: 7, message: 'Knight was to quick'},
                    {gameId: 9, message: 'Knight was to big'}
                ]);
                view.render();

                assert.equal(view.$('.card-block dd:eq(0)').html(), 7, 'Victories count should be correct');
                assert.equal(view.$('.card-block dd:eq(1)').html(), 3, 'Defeats count should be correct');
                assert.include(view.$('.card-block .text-success').text(), '70%', 'Success ratio should be displayed');

                assert.equal(view.$('.list-group-flush .list-group-item').length, 3, 'Should show defeat messages');
            });
        });
    }
);
