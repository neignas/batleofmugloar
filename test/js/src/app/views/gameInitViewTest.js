define(
    [
    'underscore',
    'chai',
    'sinon',
    'app/views/gameInitView'
    ],
    function (
        _,
        chai,
        sinon,
        GameInitView
    ) {
        "use strict";

        describe("Game initialization view", function () {

            var assert = chai.assert,
                view,
                server;

            beforeEach(function () {
                server = sinon.fakeServer.create();
                view = new GameInitView();
            });

            afterEach(function () {
                view.remove();
                server.restore();
            });

            it("should render it self", function () {
                view.render();
            });

            it("should disable elements on go to war click", function () {
                view.render();
                view.$('.go-to-war').click();

                assert.ok(view.$('.go-to-war').is(':disabled'), 'Go to war button should be disabled');
                assert.ok(view.dragonsCountElement.getAttribute('disabled'), 'Dragons count scrooler should be disabled');
                assert.ok(view.battleChunkSizeElement.getAttribute('disabled'), 'Chunk size scrooler should be disabled');
            });

            it("should show results on go to war click", function () {
                var xmlResponse = '<report>\r\n<time>Tue Aug 02 2016 09:56:45 GMT+0000 (UTC)<\/time>\r\n<coords>\r\n<x>3916.234<\/x>\r\n<y>169.914<\/y>\r\n<z>6.33<\/z>\r\n<\/coords>\r\n<code>NMR<\/code>\r\n<message>\r\nAnother day....\r\n<\/message>\r\n<varX-Rating>8<\/varX-Rating>\r\n<\/report>';
                view.render();

                view.dragonsCountElement.noUiSlider.set(2); // lets fight in two battles

                view.$('.go-to-war').click();

                assert.ok(view.$('.select-dragon').is(':disabled'), 'Dragons selector should be disabled');
                assert.ok(view.$('.go-to-war').is(':disabled'), 'Go to war button should be disabled');
                assert.ok(view.dragonsCountElement.getAttribute('disabled'), 'Dragons count scrooler should be disabled');
                assert.ok(view.battleChunkSizeElement.getAttribute('disabled'), 'Chunk size scrooler should be disabled');

                assert.equal(server.requests[0].method, 'GET');
                server.requests[0].respond(
                    200,
                    {"Content-Type": "application/json"},
                    JSON.stringify({
                            "gameId": 61546,
                            "knight": {
                                "name": "Sir. Cameron Baker of Yukon",
                                "attack": 3,
                                "armor": 2,
                                "agility": 7,
                                "endurance": 8
                            }
                        }
                    )
                );

                assert.equal(server.requests[1].method, 'GET');
                server.requests[1].respond(
                    200,
                    {"Content-Type": "application/json"},
                    JSON.stringify({
                            "gameId": 7637056,
                            "knight": {
                                "name": "Sir. Paul Pittman of Quebec",
                                "attack": 4,
                                "armor": 0,
                                "agility": 8,
                                "endurance": 8
                            }
                        }
                    )
                );

                // respond weather
                server.requests[2].respond(
                    200,
                    {"Content-Type": "application/xml"},
                    xmlResponse
                );
                server.requests[3].respond(
                    200,
                    {"Content-Type": "application/xml"},
                    xmlResponse
                );

                server.requests[4].respond(
                    200,
                    {"Content-Type": "application/json"},
                    JSON.stringify({
                            "status": "Victory",
                            "message": "Knight died in storm, dragon survived in the pen."
                        }
                    )
                );
                server.requests[5].respond(
                    200,
                    {"Content-Type": "application/json"},
                    JSON.stringify({
                            "status": "Victory",
                            "message": "Knight died in storm, dragon survived in the pen."
                        }
                    )
                );

                assert.notOk(view.$('.select-dragon').is(':disabled'), 'Dragons selector should be enabled');
                assert.notOk(view.$('.go-to-war').is(':disabled'), 'Go to war button should be enabled');
                assert.notOk(view.dragonsCountElement.getAttribute('disabled'), 'Dragons count scrooler should be enabled');
                assert.notOk(view.battleChunkSizeElement.getAttribute('disabled'), 'Chunk size scrooler should be enabled');

                assert.equal(view.$('.progress-animated').attr('value'), 100, 'Progress bar should be completed');
                assert.ok(view.$('.game-results .card-block').length > 0, 'Game results should be shown');
            });
        });
    }
);
