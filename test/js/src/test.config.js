require.config({
    baseUrl: "../../../js/src",
    paths: {
        jquery: '../vendor/jquery/jquery.min',
        bootstrap: '../vendor/bootstrap/bootstrap.min',
        underscore: '../vendor/underscore/underscore-min',
        backbone: '../vendor/backbone/backbone-min',
        text: "../vendor/text/text",
        chai: "../../test/node_modules/chai/chai",
        sinon: "../../test/node_modules/sinon/lib/sinon",
        nouislider: "../vendor/nouislider/nouislider.min",
        wNumb: "../vendor/wnumb/wNumb"
    },
    shim: {
        jquery: {
            exports: '$'
        },
        bootstrap: {
            deps: ['jquery']
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore']
        },
        fitText: {
            deps: ['jquery']
        },
        chai: ['underscore']
    }
});

define('routes', {
    battleUrl: 'battle_url_mock',
    solveBattleUrl: 'solve_battle_url_mock',
    weatherUrl: 'weather_url_mock'
});

var test_root = '../../test/js/src/';

require([
    test_root + 'app/views/gameInitViewTest',
    test_root + 'battle/helpers/battleHelperTest',
    test_root + 'battle/views/dragonsChooserViewTest',
    test_root + 'battle/views/gameResultsViewTest'
], function() {
    if (typeof window.mochaPhantomJS !== "undefined") { window.mochaPhantomJS.run(); }
    else { window.mocha.run(); }
});
